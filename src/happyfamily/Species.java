package happyfamily;

import java.util.Arrays;

public enum Species {

    DOG(false,4,true),CAT(false,4,true),BUNNY(false,4,true),MONKEY(false,4,true),FISH(false,0,false);

   private final boolean canfly;

   private final int numberOfLegs;

   private final boolean hasFur;

    Species(boolean canFly, int numberOfLegs,boolean hasFur) {
        this.canfly= canFly;
        this.numberOfLegs= numberOfLegs;
        this.hasFur= hasFur;
    }

    public boolean isCanfly() {
        return canfly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    @Override
    public String toString() {
        return super.toString()+"{" +
                "canfly=" + canfly +
                ", numberOfLegs=" + numberOfLegs +
                ", hasFur=" + hasFur +
                '}';
    }
}
